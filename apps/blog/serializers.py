from rest_framework import serializers
from .models import Post
from apps.category.serializers import CategorySerializer

class PostSerializer(serializers.ModelSerializer):
    image=serializers.CharField(source="get_image")
    video=serializers.CharField(source="get_video")

    category = CategorySerializer()
    class Meta:
        model = Post
        fields = [
            "blog_uuid",
            "title",
            "slug",
            "image",
            "video",
            "description",
            "excerpt",
            "published_date",
            "status",
            "category",
        ]