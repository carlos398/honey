import uuid
from django.db import models
from django.utils import timezone
from apps.category.models import Category


def blog_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'blog/{0}/{1}'.format(instance.user.id, filename)


class Post(models.Model):

    class PostObjects(models.Manager):
        def get_queryset(self):
            return super().get_queryset().filter(status='published')

    class StatusChoices(models.TextChoices):
        draft = 'draft', 'Draft'
        published = 'published', 'Published'

    blog_uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    image = models.ImageField(upload_to=blog_directory_path, blank=True)
    video = models.FileField(blank=True)
    description = models.TextField(blank=True)
    excerpt = models.TextField(blank=True)

    # author = models.CharField(max_length=255, blank=True)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, blank=True, null=True)

    published_date = models.DateTimeField(default=timezone.now)
    status = models.CharField( max_length=10, choices=StatusChoices.choices, default=StatusChoices.draft)

    objects = models.Manager()
    postObjects = PostObjects()

    class Meta:
        ordering = ['-published_date']
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'
    
    def __str__(self) -> str:
        return self.title
    
    def get_image(self):
        if self.image:
            return self.image.url
        return None
    
    def get_video(self):
        if self.video:
            return self.video.url
        return None
